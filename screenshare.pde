ArrayList<point> points = new ArrayList<point>();void setup(){size(1920,1080,P3D);for(int i=0;i<400;i++)points.add(new point(random(0,1000),random(0,1000),random(-2,2),random(-2,2)));frameRate(60);}
double getDistance(point a,point b){return sqrt((float)((a.xPos - b.xPos)*(a.xPos - b.xPos)+(a.yPos - b.yPos)*(a.yPos - b.yPos)));}
double getRawDistance(point a,point b){return ((a.xPos - b.xPos)*(a.xPos - b.xPos)+(a.yPos - b.yPos)*(a.yPos - b.yPos));}class point {
public double xPos, yPos,xVel,yVel;point(double xPos1,double yPos1,double xVel1,double yVel1){xPos = xPos1;yPos = yPos1;xVel = xVel1;yVel = yVel1;}
public void applyStep(){xPos += xVel;yPos += yVel;if(xPos>1000 || xPos<0) xVel *=-1;if(yPos>1000 || yPos<0) yVel*=-1;}public void draws(){
color(255,255,255);ellipse((float)xPos,(float)yPos,1,1);}public void drawLines(){for(point p : points){if(!this.equals(p)){double a = getDistance(this,p),raw = getRawDistance(this,p);
float fa = 255 - ((float) a*2.55);if(raw<10000){stroke(fa);line((float)this.xPos,(float)this.yPos,(float)p.xPos,(float)p.yPos);}}}}}void draw(){background(0,0,0);
point a = new point(mouseX,mouseY,0,0);points.add(a);for(point p : points){p.applyStep();p.draws();p.drawLines();}points.remove(a);}
